#!/bin/bash
#SBATCH --job-name=my_job_mult      # Specify job name
#SBATCH --partition=shared     # Specify partition name
#SBATCH --ntasks=1
#SBATCH --array=0-44
#SBATCH --time=02:00:00        # Set a limit on the total run time
#SBATCH --mail-type=FAIL       # Notify user by email in case of job failure
#SBATCH --account=mh0010       # Charge resources on this project account
#SBATCH --output=./Data/output_files/my_job_mult.o%j    # File name for standard output

set -e
ulimit -s 204800

# Print the task id.
echo "My SLURM_ARRAY_TASK_ID: " $SLURM_ARRAY_TASK_ID

module load python3
pip install cdsapi

python -u /work/mh0731/m300609/myworkfolder/myworkfolder/Ship_campaign/orcestra/era5UsingCdsApiMultMonth.py $SLURM_ARRAY_TASK_ID

