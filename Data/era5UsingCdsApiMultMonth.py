import cdsapi
import sys
import os

yearsAndMonths = [(f'{year:04}', f'{month:02}') for year in range(2001, 2023) for month in range(8,10)]
year, month = yearsAndMonths[int(sys.argv[1])]

#print(year, month)

output_file_path = f'/work/mh0731/m300609/myworkfolder/myworkfolder/Ship_campaign/orcestra/Data/era5_{year}_{month}.nc'

if os.path.exists(output_file_path):
    print(f"The file {output_file_path} already exists. Terminating the program.")
    sys.exit()
else:
    print(f"The file {output_file_path} does not exist. Continuing with the program.")
    # Your program logic can continue here

c = cdsapi.Client()
c.retrieve(
    'reanalysis-era5-single-levels',
    {
        'product_type': 'reanalysis',
        'format': 'netcdf',
        'variable': [
            'total_column_water_vapour', 'total_precipitation'
        ],
        'year': year,
        'month': month,
        'day': [
            '01', '02', '03',
            '04', '05', '06',
            '07', '08', '09',
            '10', '11', '12',
            '13', '14', '15',
            '16', '17', '18',
            '19', '20', '21',
            '22', '23', '24',
            '25', '26', '27',
            '28', '29', '30',
            '31',
        ],
        'time': [
            '00:00', '01:00', '02:00',
            '03:00', '04:00', '05:00',
            '06:00', '07:00', '08:00',
            '09:00', '10:00', '11:00',
            '12:00', '13:00', '14:00',
            '15:00', '16:00', '17:00',
            '18:00', '19:00', '20:00',
            '21:00', '22:00', '23:00',
        ],
        'area': [
            25, -65, -10,
            0,
        ],
    },
    output_file_path)

#'10m_u_component_of_wind', '10m_v_component_of_wind', '2m_dewpoint_temperature', '2m_temperature', 
#'convective_available_potential_energy', 'sea_surface_temperature', 'skin_temperature', 'surface_pressure',
#'total_column_water', 'total_column_water_vapour', 
#'convective_inhibition', 'top_net_solar_radiation', 'top_net_thermal_radiation', 'total_precipitation',